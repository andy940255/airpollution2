/* eslint-disable */

function g2_1_start() {
    $('.game2-1').remove();
    
    
    
    

    g2_1_load();
}

function g2_1_load() {
    //map
    $('#box').append('<div id="g2-1-mapbox" class="game2-1"><img id="g2-1-map" class="game2-1" src="image/game2-1/map/map.jpg"></div>');
    
    //bgframe
    $('#box').append('<img id="g2-1-bgframe" class="game2-1" src="image/game2-1/map/bgframe.png">');
    
    //scroll box
    $('#box').append('<div id="g2-1-scrollbox" class="game2-1"></div>');
    
//factorybox
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-01" class="game2-1"><img id="g2-1-factory-01" class="game2-1"   src="image/game2-1/factory/1-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-02" class="game2-1"><img id="g2-1-factory-02" class="game2-1"   src="image/game2-1/factory/5-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-03" class="game2-1"><img id="g2-1-factory-03" class="game2-1"   src="image/game2-1/factory/2-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-04" class="game2-1"><img id="g2-1-factory-04" class="game2-1"   src="image/game2-1/factory/3-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-05" class="game2-1"><img id="g2-1-factory-05" class="game2-1"   src="image/game2-1/factory/4-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-06" class="game2-1"><img id="g2-1-factory-06" class="game2-1"   src="image/game2-1/factory/2-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-07" class="game2-1"><img id="g2-1-factory-07" class="game2-1"   src="image/game2-1/factory/6-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-08" class="game2-1"><img id="g2-1-factory-08" class="game2-1"   src="image/game2-1/factory/8-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-09" class="game2-1"><img id="g2-1-factory-09" class="game2-1"   src="image/game2-1/factory/6-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-10" class="game2-1"><img id="g2-1-factory-10" class="game2-1"   src="image/game2-1/factory/8-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-11" class="game2-1"><img id="g2-1-factory-11" class="game2-1"   src="image/game2-1/factory/1-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-12" class="game2-1"><img id="g2-1-factory-12" class="game2-1"   src="image/game2-1/factory/8-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-13" class="game2-1"><img id="g2-1-factory-13" class="game2-1"   src="image/game2-1/factory/5-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-14" class="game2-1"><img id="g2-1-factory-14" class="game2-1"   src="image/game2-1/factory/4-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-15" class="game2-1"><img id="g2-1-factory-15" class="game2-1"   src="image/game2-1/factory/9-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-16" class="game2-1"><img id="g2-1-factory-16" class="game2-1"   src="image/game2-1/factory/3-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-17" class="game2-1"><img id="g2-1-factory-17" class="game2-1"   src="image/game2-1/factory/5-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-18" class="game2-1"><img id="g2-1-factory-18" class="game2-1"   src="image/game2-1/factory/6-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-19" class="game2-1"><img id="g2-1-factory-19" class="game2-1"   src="image/game2-1/factory/8-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-20" class="game2-1"><img id="g2-1-factory-20" class="game2-1"   src="image/game2-1/factory/6-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-21" class="game2-1"><img id="g2-1-factory-21" class="game2-1"   src="image/game2-1/factory/8-w.png"></div>');
    $('#g2-1-mapbox').append('<div id="g2-1-factorybox-22" class="game2-1"><img id="g2-1-factory-22" class="game2-1"   src="image/game2-1/factory/7-w.png"></div>');
    //factory
    //factory cover 
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-01" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-02" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-03" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-04" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-05" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-06" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-07" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-08" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-09" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-10" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-11" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-12" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-13" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-14" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-15" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-16" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-17" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-18" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-19" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-20" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-21" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	$('#g2-1-mapbox').append('<div id="g2-1-factoryboxcover-22" class="game2-1" onmouseover="g2_1_mouseover(event)" onmouseout="g2_1_mouseout(event)"></div>');
	
	
    //map scrolling
    const slider = document.querySelector('#g2-1-mapbox');
    const screen = document.querySelectorAll('#g2-1-scrollbox, #g2-1-mapbox');
    let isDown = false;
    let startX;
    let startY;
    let scrollLeft;
    let scrollTop;

	for(let i=0;i<screen.length;i++){
		screen[i].addEventListener('mousedown', (e) => {
        isDown = true;
        slider.classList.add("active");
        startY = e.pageY - slider.offsetTop;
        scrollTop = slider.scrollTop;

    });
    
    screen[i].addEventListener('mouseleave', () => {
        isDown = false;
        slider.classList.remove("active");
    });
    
    screen[i].addEventListener('mouseup', () => {
        isDown = false;
        slider.classList.remove("active");
    });
    screen[i].addEventListener('mousemove', (e) => {
        if (!isDown) return;
        e.preventDefault();

        const y = e.pageY - slider.offsetTop;
        const walkY = (y - startY);
        
        if (scrollTop - walkY >=0 && scrollTop - walkY <= 1300){
            slider.scrollTop = scrollTop - walkY;
        }

    });
	//onwheel
	screen[i].addEventListener('wheel', (e) => {
		var walkY = e.deltaY;
		
		 if (slider.scrollTop + walkY >=0 && slider.scrollTop +  walkY < 2100){
            slider.scrollTop = slider.scrollTop + walkY;
        }
	});
	}
    
    //img drag prohibit
	 $('.game2-1').attr('draggable', 'false');
}

function g2_1_mouseover(e){
	e.preventDefault();
	var ObjID= e.target.id.replace("g2-1-factoryboxcover-", "g2-1-factorybox-");
	var Obj = document.getElementById(ObjID).firstChild;
	Obj.src=Obj.src.replace("-w.png","-r.png");
	$('#'+Obj.parentElement.id).append('<img id="g2-1-lock" src="image/game2-1/lock.png">');
}

function g2_1_mouseout(e){
	e.preventDefault();
	var ObjID= e.target.id.replace("g2-1-factoryboxcover-", "g2-1-factorybox-");
	var Obj = document.getElementById(ObjID).firstChild;
	$('#g2-1-lock').remove();
	Obj.src=Obj.src.replace("-r.png","-w.png");
}
