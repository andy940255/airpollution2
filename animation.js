/* eslint-disable */

var nowPage;

$(document).ready(function() {
    startAnimation();
})

function startAnimation() {
     //monster animation
    $('#sp-monster1').animate({left: "125px"}, 2500).animate({left: "105px"}, 2500);
    $('#sp-monster2').animate({top: "395px"}, 1700).animate({top: "415px"}, 1700);
    $('#sp-monster3').animate({left: "890px"}, 2900).animate({left: "870px"}, 2900);
    $('#sp-monster4').animate({top: "330px"}, 2200).animate({top: "350px"}, 2200);
    //smoke animation
    $('#sp-smoke1').animate({top: "-3px"}, 1900).animate({top: "17px"}, 1900);
    $('#sp-smoke2').animate({top: "-5px"}, 2800).animate({top: "9px"}, 2800);
    $('#sp-smoke3').animate({top: "45px"}, 2400).animate({top: "55px"}, 2400);
    $('#sp-smoke4').animate({top: "30px"}, 1800).animate({top: "50px"}, 1800);
    
    setTimeout(startAnimation,3400);
}

