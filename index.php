<?php
header("Content-Type:text/html; charset=utf-8");
session_start();
require('db.php');

$isGuest=0;//否
$sqlSearch=0;
$timeCheck=0;//否

//判斷是否有token進來，若有責檢查此token是否在gis的db中
if(isset($_POST['token'])){
    $postToken=$_POST['token'];
    $sqlSearch=1;
    $timeCheck=1;
    $sqlInstruction="SELECT * FROM	`ACCOUNT_ONLINE` WHERE `Token`='$postToken'";
}
else {
    //沒有token過來，檢查當前是否有session SN 及 token，若有則檢查這個token是否在gis的db中，若無則當作遊客
    if(isset($_SESSION['SN']) && isset($_SESSION['token'])){
        $postToken=$_SESSION['token'];
        $sqlSearch=1;
        $sqlInstruction="SELECT * FROM	`ACCOUNT_ONLINE` WHERE `Token`='$postToken'";
    }else{
        $isGuest=1;
    }
}

//判斷需不需要進入gis的db查詢使用者資訊
if($sqlSearch==1){    
    $searchResult=$mysqliG->query($sqlInstruction);
    $searchArray= mysqli_fetch_array($searchResult);
}

//判斷需要不要檢查時間(在10秒內登入)
if($timeCheck==1){
    $sendTime=$searchArray['TimeEnter'];
    if((strtotime($catchTime) - strtotime($sendTime))<10 && $searchArray['Allow']==1){
       $signSituation=1;
       $mysqliG->query("UPDATE `ACCOUNT_ONLINE` SET `Allow` = '0' WHERE `Token` = '$postToken'");
    }
}

//檢查是否需要執行sql查詢指令
if($sqlSearch==1){
    //檢查結果，>0表示有找到使用者資料
    if($searchResult->num_rows>0){
        if($signSituation==1){
            $_SESSION['SN']=$searchArray['ACCOUNT_SN'];
            $_SESSION['token']=$searchArray['Token'];
            $_SESSION['name']=$searchArray['ACCOUNT_NAME']; 
        }
    }else{
        $isGuest=1;
        session_unset();
    }
}

//檢查是否為遊客，若不是，則判斷是否曾經進過ap2
if($isGuest==1){
    $name='遊客';
}else if ($isGuest==0){
    $SN=$_SESSION['SN'];
    $searchResult=$mysqli->query("SELECT * FROM `ap2` WHERE `ACCOUNT_SN` = '$SN'");
    if($searchResult->num_rows<=0){
        $mysqli->query("INSERT INTO `ap2`(`ACCOUNT_SN`) VALUES (N'$SN')");
    }
    $name=$_SESSION['name'];
}

echo '<script>console.log("' . $name . '")</script>';

?>

<!DOCTYPE html>

<html lang="zh-TW">

<head>
    <title>Air Pollution 2</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="main.css">
</head>

<script src="jquery.js" type="text/javascript"></script>
<script src="main.js" type="text/javascript"></script>
<script src="startPage.js" type="text/javascript"></script>
<script src="game1-1.js" type="text/javascript"></script>
<script src="game1-2.js" type="text/javascript"></script>
<script src="game2-1.js" type="text/javascript"></script>
<script src="animation.js" type="text/javascript"></script>
<script src="timeCounter3.js" type="text/javascript"></script>
<script src="timeCounter10.js" type="text/javascript"></script>
<script src="timeCounter15.js" type="text/javascript"></script>

<body id="box">
</body>

</html>
