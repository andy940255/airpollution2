/* eslint-disable */


var g1_2_level;
var g1_2_q_arr = ['1', '2', '3', '4'];
var g1_2_monsters = ['1', '2', '3', '4'];
var g1_2_sum_right;
var g1_2_sum_left;

var g1_2_level;
var g1_2_correct_times;
var g1_2_level_grade;
var g1_2_save_time;
var g1_2_time_grade;
var g1_2_total_grade;

function g1_2_start() {
    $('.game1-2').remove();
    g1_2_reset_image();

    g1_2_level = -1;
    g1_2_correct_times = 0;
    g1_2_level_grade = 0;
    g1_2_save_time = 0;
    g1_2_time_grade = 0;
    g1_2_total_grade = 0;

    shuffle(g1_2_q_arr);
    shuffle(g1_2_monsters);

    g1_2_load();

    g1_2_start_levels();
}

function g1_2_load() {
    //bg
    $('#box').append('<img id="g1-2-bg" class="game1-2" src="image/game1-2/bg.png">');
    //time
    $('#box').append('<img id="g-timeCounter" class="game1-2" src="image/timecounter/counter.png">');
    $('#box').append('<div id="g-timeCounterNum" class="game1-2"></div>');
    //correct|wrong
    $('#box').append('<div id="g1-2-leftbox" class="game1-2"></div>');
    $('#box').append('<div id="g1-2-rightbox" class="game1-2"></div>');
    //grade
    $('#box').append('<div id="g1-2-grade" class="game1-1"></div>');
    $('#box').append('<div id="g1-2-standard" class="game1-1"></div>');
}

function g1_2_start_levels() {
    var T1, T2, T3;
    g1_2_level++;

    if (g1_2_level > 3) {
        g1_2_load_result();
        return;
    }
    
    document.getElementById('g1-2-grade').innerHTML = g1_2_level_grade;
    document.getElementById('g1-2-standard').innerHTML = 6000;

    //開始
    startCounter10();
    g1_2_get_hint();
    T1 = setTimeout(function () {
        //10秒後
        //移除完整圖
        g1_2_reset_image();
        g1_2_get_question();

        //donekey
        $('#box').append('<img id="g1-2-donekey" class="game1-2" src="image/game1-2/donekey.png">');

        $('#g1-2-donekey').on('click', function () {
            //donkey移除
            $('#g1-2-donekey').remove();
            //元件上鎖
            $('.game1-2-q').attr('draggable', 'false');
            //left&rightbox z-index up to 10
            $('#g1-2-leftbox').css('z-index', '10');
            $('#g1-2-rightbox').css('z-index', '10');
            counterStop15();
            clearTimeout(T2);
            startCounter3();
            g1_2_check();
            T3 = setTimeout(function () {
                //3秒後
                g1_2_reset_image();
                g1_2_start_levels();
            }, 4000)
        });


        startCounter15();
//        console.log('pass 10s');
        T2 = setTimeout(function () {
            //15秒後
            //startCounter15(); 貝殼不信
            //donkey移除
            $('#g1-2-donekey').remove();
            //元件上鎖
            $('#game1-2-q').attr('draggable', 'false');
            //left&rightbox z-index up to 3
            $('#g1-2-leftbox').css('z-index', '3');
            $('#g1-2-rightbox').css('z-index', '3');
            g1_2_check();
            startCounter3();
            T3 = setTimeout(function () {
                //3秒後
                g1_2_reset_image();
                g1_2_start_levels();
            }, 4000)
        }, 16000)
    }, 11000);

    g1_2_sum_right = 0;
    g1_2_sum_left = 0;

}

//計數器 判斷左右對錯用
function g1_2_levels_sum(imgID) {
    tmp = imgID.charAt(imgID.length - 1);
    switch (g1_2_q_arr[g1_2_level]) {
        case '1':
            if (tmp == '1' || tmp == '4' || tmp == '7') {
                g1_2_sum_left++;
            } else {
                g1_2_sum_right++;
            }
            break;
        case '2':
            if (tmp == '2' || tmp == '4' || tmp == '5') {
                g1_2_sum_left++;
            } else {
                g1_2_sum_right++;
            }
            break;
        case '3':
            if (tmp == '1' || tmp == '4' || tmp == '6' || tmp == '7') {
                g1_2_sum_left++;
            } else {
                g1_2_sum_right++;
            }
            break;
        case '4':
            if (tmp == '1' || tmp == '3' || tmp == '5') {
                g1_2_sum_left++;
            } else {
                g1_2_sum_right++;
            }
            break;
    }
}

//判斷小題對錯
function g1_2_check() {
    switch (g1_2_q_arr[g1_2_level]) {
        case '1':
            if (g1_2_sum_left == 3) {
                $('#g1-2-leftbox').append('<img id="g1-2-left-correct" class="game1-2" src="image/game1-2/correct.png">');
                g1_2_correct();
            } else {
                $('#g1-2-leftbox').append('<img id="g1-2-left-wrong" class="game1-2" src="image/game1-2/wrong.png">');
            }
            if (g1_2_sum_right == 5) {
                $('#g1-2-rightbox').append('<img id="g1-2-right-correct" class="game1-2" src="image/game1-2/correct.png">');
                g1_2_correct();
            } else {
                $('#g1-2-rightbox').append('<img id="g1-2-right-wrong" class="game1-2" src="image/game1-2/wrong.png">');
            }
            break;
        case '2':
            if (g1_2_sum_left == 3) {
                $('#g1-2-leftbox').append('<img id="g1-2-left-correct" class="game1-2" src="image/game1-2/correct.png">');
                g1_2_correct();
            } else {
                $('#g1-2-leftbox').append('<img id="g1-2-left-wrong" class="game1-2" src="image/game1-2/wrong.png">');
            }
            if (g1_2_sum_right == 3) {
                $('#g1-2-rightbox').append('<img id="g1-2-right-correct" class="game1-2" src="image/game1-2/correct.png">');
                g1_2_correct();
            } else {
                $('#g1-2-rightbox').append('<img id="g1-2-right-wrong" class="game1-2" src="image/game1-2/wrong.png">');
            }
            break;
        case '3':
            if (g1_2_sum_left == 4) {
                $('#g1-2-leftbox').append('<img id="g1-2-left-correct" class="game1-2" src="image/game1-2/correct.png">');
                g1_2_correct();
            } else {
                $('#g1-2-leftbox').append('<img id="g1-2-left-wrong" class="game1-2" src="image/game1-2/wrong.png">');
            }
            if (g1_2_sum_right == 3) {
                $('#g1-2-rightbox').append('<img id="g1-2-right-correct" class="game1-2" src="image/game1-2/correct.png">');
                g1_2_correct();
            } else {
                $('#g1-2-rightbox').append('<img id="g1-2-right-wrong" class="game1-2" src="image/game1-2/wrong.png">');
            }
            break;
        case '4':
            if (g1_2_sum_left == 3) {
                $('#g1-2-leftbox').append('<img id="g1-2-left-correct" class="game1-2" src="image/game1-2/correct.png">');
                g1_2_correct();
            } else {
                $('#g1-2-leftbox').append('<img id="g1-2-left-wrong" class="game1-2" src="image/game1-2/wrong.png">');
            }
            if (g1_2_sum_right == 3) {
                $('#g1-2-rightbox').append('<img id="g1-2-right-correct" class="game1-2" src="image/game1-2/correct.png">');
                g1_2_correct();
            } else {
                $('#g1-2-rightbox').append('<img id="g1-2-right-wrong" class="game1-2" src="image/game1-2/wrong.png">');
            }
            break;
    }
}

function g1_2_correct() {
    g1_2_save_time += (time15 / 2);
    g1_2_correct_times++;
    g1_2_level_grade += 1000;
}

function g1_2_load_result() {
    $('.game1-2').remove();
    $('.game1-2-q').remove();
    $('.game1-2-h').remove();
    
    g1_2_save_time = Math.floor(g1_2_save_time);
    
    //時間殘餘計算
    if (g1_2_save_time >= 50) {
        g1_2_time_grade = 2300;
    } else if (g1_2_save_time < 50 && g1_2_save_time >= 40) {
        g1_2_time_grade = 1700;
    } else if (g1_2_save_time < 40 && g1_2_save_time >= 30) {
        g1_2_time_grade = 1200;
    } else if (g1_2_save_time < 30 && g1_2_save_time >= 20) {
        g1_2_time_grade = 800;
    } else if (g1_2_save_time < 20 && g1_2_save_time >= 10) {
        g1_2_time_grade = 500;
    }
    
    g1_2_total_grade = g1_2_level_grade + g1_2_time_grade;
    var g1_2_monster = g1_2_monsters[0];
    var g1_2_isSuccess = false;
    
    // 通關標準判斷
    if (g1_2_total_grade >= 6000) {
        //通關
        g1_2_isSuccess = true;
        
        $('#box').append('<img id="g1-2result-bg" class="game1-2" src="image/game1-2/result/correct-bg.png">');
        $('#box').append('<img id="g1-2result-correct-exit" class="game1-2" src="image/game1-2/result/correct-exit.png">');
        $('#box').append('<img id="g1-2result-correct-frame" class="game1-2" src="image/game1-2/result/correct-frame.png">');
        $('#box').append('<img id="g1-2result-correct-mon" class="game1-2" src="image/game1-2/result/correct-1.png">');
        $('#box').append('<img id="g1-2result-correct" class="game1-2" src="image/game1-2/result/correct.png">');
        $('#box').append('<img id="g1-2result-correct-retry" class="game1-2" src="image/game1-2/result/retry.png">');
        $('#box').append('<img id="g1-2result-correct-next" class="game1-2" src="image/game1-2/result/next.png">');
        // text
        $('#box').append('<div id="g1-2result-correct-t1" class="game1-2">' + g1_2_correct_times + '</div>');
        $('#box').append('<div id="g1-2result-correct-t2" class="game1-2">' + '+' + g1_2_level_grade + '</div>');
        $('#box').append('<div id="g1-2result-correct-t3" class="game1-2">' + g1_2_save_time + '</div>');
        $('#box').append('<div id="g1-2result-correct-t4" class="game1-2">' + '+' + g1_2_time_grade + '</div>');
        $('#box').append('<div id="g1-2result-correct-t5" class="game1-2">' + g1_2_total_grade + '</div>');
        $('#box').append('<div id="g1-2result-correct-t6" class="game1-2">6000</div>');
        
        //correct
        $('#g1-2result-correct-retry').on('click', function () {
            $('.game1-2').remove();
            $('.game1-2-q').remove();
            $('.game1-2-h').remove();
            g1_1_start();
        });
//        $('#g1-2result-correct-next').on('click', function () {
//            $('.game1-2').remove();
//            $('.game1-2-q').remove();
//            $('.game1-2-h').remove();
//            g1_2_start();
//        });
        $('#g1-2result-correct-exit').on('click', function () {
            $('.game1-2').remove();
            $('.game1-2-q').remove();
            $('.game1-2-h').remove();
            sp_start();
        });
    } else {
        //失敗
        $('#box').append('<img id="g1-2monster-' + g1_2_monster + '"  class="game1-2" src="image/game1-2/result/wrong-ms-' + g1_2_monster + '.png">');
        g1_2_wrong_show(g1_2_monster);
        
        $('#box').append('<video id="g1-2resultw-bg" class="game1-2" autoplay="autoplay"> <source src="image/game1-2/result/wrong-bg-2.mp4" type="video/mp4"/></video>');
        //wrong-show
        $('#box').append('<img id="g1-2wrong-show" class="game1-2" src="image/game1-2/result/wrong-show.png">');
        
        setTimeout(function() {
            $('#g1-2monster-' + g1_2_monster).animate({left: "30px"}, 1000)
            $('#g1-2wrong-show').remove();
            $('#box').append('<img id="g1-2result-wrong-frame" class="game1-2" src="image/game1-2/result/wrong-frame.png">');
            $('#box').append('<img id="g1-2result-wrong-exit" class="game1-2" src="image/game1-2/result/correct-exit.png">');
            $('#box').append('<img id="g1-2result-wrong-retry" class="game1-2" src="image/game1-2/result/retry.png">');
            // text
            $('#box').append('<div id="g1-2result-wrong-t1" class="game1-2">' + g1_2_correct_times + '</div>');
            $('#box').append('<div id="g1-2result-wrong-t2" class="game1-2">' + '+' + g1_2_level_grade + '</div>');
            $('#box').append('<div id="g1-2result-wrong-t3" class="game1-2">' + g1_2_save_time + '</div>');
            $('#box').append('<div id="g1-2result-wrong-t4" class="game1-2">' + '+' + g1_2_time_grade + '</div>');
            $('#box').append('<div id="g1-2result-wrong-t5" class="game1-2">' + g1_2_total_grade + '</div>');
            $('#box').append('<div id="g1-2result-wrong-t6" class="game1-2">6000</div>');
            
            //wrong
            $('#g1-2result-wrong-retry').on('click', function () {
                $('.game1-2').remove();
                $('.game1-2-q').remove();
                $('.game1-2-h').remove();
                g1_2_start();
            });
            $('#g1-2result-wrong-exit').on('click', function () {
                $('.game1-2').remove();
                $('.game1-2-q').remove();
                $('.game1-2-h').remove();
                sp_start();
            });
            }, 3400)
    }
    
    //分數判斷，傳回db
    var progress = 2;
    $.ajax({
        //傳入值設定
        type:'GET',
        url:'upload.php',
        data:{progress: progress, 
             grade: g1_2_total_grade, 
             isSuccess: g1_2_isSuccess},
        //回傳值設定
        dataType: "text",
        success: function(data){
//                console.log(data);
        }
    });
}

function g1_2_wrong_show(num){
    switch(num){
        case '1':
            $('#g1-2monster-1').animate({top: "130px"}, 1700).animate({top: "70px"}, 1700);
        case '2':
            $('#g1-2monster-2').animate({top: "100px"}, 1700).animate({top: "40px"}, 1700);
        case '3':
            $('#g1-2monster-3').animate({top: "250px"}, 1700).animate({top: "100px"}, 1700);
        case '4':
            $('#g1-2monster-4').animate({top: "90px"}, 1700).animate({top: "55px"}, 1700);
    }
    setTimeout(function(){
        g1_2_wrong_show(num);
    },3500);
}

//移除小題 清空上下圖示 錯誤正確框重製
function g1_2_reset_image() {
    $('.game1-2-q').remove();
    $('.game1-2-h').remove();
    $('#g1-2-left-correct').remove();
    $('#g1-2-left-wrong').remove();
    $('#g1-2-right-correct').remove();
    $('#g1-2-right-wrong').remove();
    $('#g1-2-leftbox').css('z-index', '0');
    $('#g1-2-rightbox').css('z-index', '0');
}

function g1_2_get_hint() {
    switch (g1_2_q_arr[g1_2_level]) {
        case '1':
            $('#box').append('<img id="g1-2-chemistry-hint" class="game1-2-h" src="image/game1-2/chemistry-up-0.png">');
            $('#box').append('<img id="g1-2-electronic-hint" class="game1-2-h" src="image/game1-2/electronic-up-0.png">');
            break;
        case '2':
            $('#box').append('<img id="g1-2-feed-hint" class="game1-2-h" src="image/game1-2/feed-up-0.png">');
            $('#box').append('<img id="g1-2-hospital-hint" class="game1-2-h" src="image/game1-2/hospital-up-0.png">');
            break;
        case '3':
            $('#box').append('<img id="g1-2-nationaldefense-hint" class="game1-2-h" src="image/game1-2/nationaldefense-up-0.png">');
            $('#box').append('<img id="g1-2-poison-hint" class="game1-2-h" src="image/game1-2/poison-up-0.png">');
            break;
        case '4':
            $('#box').append('<img id="g1-2-steel-hint" class="game1-2-h" src="image/game1-2/steel-up-0.png">');
            $('#box').append('<img id="g1-2-waste-hint" class="game1-2-h" src="image/game1-2/waste-up-0.png">');
    }
}

//獲取小題圖示
function g1_2_get_question() {
    switch (g1_2_q_arr[g1_2_level]) {
        case '1':
            // bg
            $('#box').append('<img id="g1-2-chemistry-bg" class="game1-2-q" src="image/game1-2/chemistry-up-bg.png">');
            $('#box').append('<img id="g1-2-electronic-bg" class="game1-2-q" src="image/game1-2/electronic-up-bg.png">');
            // sel
            $('#box').append('<div id="g1-2-sel-1-1" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-chemistry-1" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/chemistry-down-1.png"></div>');
            $('#box').append('<div id="g1-2-sel-1-2" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-electronic-1" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/electronic-down-1.png"></div>');
            $('#box').append('<div id="g1-2-sel-1-3" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-electronic-2" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/electronic-down-2.png"></div>');
            $('#box').append('<div id="g1-2-sel-1-4" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-chemistry-3" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/chemistry-down-3.png"></div>');
            $('#box').append('<div id="g1-2-sel-1-5" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-electronic-3" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/electronic-down-3.png"></div>');
            $('#box').append('<div id="g1-2-sel-1-6" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-electronic-4" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/electronic-down-4.png"></div>');
            $('#box').append('<div id="g1-2-sel-1-7" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-chemistry-2" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/chemistry-down-2.png"></div>');
            $('#box').append('<div id="g1-2-sel-1-8" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-electronic-5" class="game1-2" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/electronic-down-5.png"></div>');
            // ans
            $('#box').append('<div id="g1-2-ans-1-001" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-1-004" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-1-007" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-1-002" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-1-003" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-1-005" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-1-006" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-1-008" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            // ans double cover
            $('#box').append('<div id="g1-2-ans-1-014" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-1-023" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-1-077" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-1-028" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            break;
        case '2':
            // bg
            $('#box').append('<img id="g1-2-feed-bg" class="game1-2-q" src="image/game1-2/feed-up-bg.png">');
            $('#box').append('<img id="g1-2-hospital-bg" class="game1-2-q" src="image/game1-2/hospital-up-bg.png">');
            // sel
            $('#box').append('<div id="g1-2-sel-2-1" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-hospital-1" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/hospital-down-1.png"></div>');
            $('#box').append('<div id="g1-2-sel-2-2" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-feed-1" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/feed-down-1.png"></div>');
            $('#box').append('<div id="g1-2-sel-2-3" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-hospital-2" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/hospital-down-2.png"></div>');
            $('#box').append('<div id="g1-2-sel-2-4" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-feed-2" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/feed-down-2.png"></div>');
            $('#box').append('<div id="g1-2-sel-2-5" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-feed-3" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/feed-down-3.png"></div>');
            $('#box').append('<div id="g1-2-sel-2-6" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-hospital-3" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/hospital-down-3.png"></div>');
            // ans
            $('#box').append('<div id="g1-2-ans-2-002" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-2-004" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-2-005" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-2-001" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-2-003" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-2-006" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            // ans double cover
            $('#box').append('<div id="g1-2-ans-2-022" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-2-044" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-2-016" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-2-036" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            break;
        case '3':
            // bg
            $('#box').append('<img id="g1-2-nationaldefense-bg" class="game1-2-q" src="image/game1-2/nationaldefense-up-bg.png">');
            $('#box').append('<img id="g1-2-poison-bg" class="game1-2-q" src="image/game1-2/poison-up-bg.png">');
            //sel
            $('#box').append('<div id="g1-2-sel-3-1" class="game1-2-q"  data-check="0" ondragover="false" ondrop="false"" ><img id="g1-2-nationaldefense-3" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/nationaldefense-down-3.png"></div>');
            $('#box').append('<div id="g1-2-sel-3-2" class="game1-2-q"  data-check="0" ondragover="false" ondrop="false"" ><img id="g1-2-poison-2" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/poison-down-2.png"></div>');
            $('#box').append('<div id="g1-2-sel-3-3" class="game1-2-q"  data-check="0" ondragover="false" ondrop="false"" ><img id="g1-2-poison-1" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/poison-down-1.png"></div>');
            $('#box').append('<div id="g1-2-sel-3-4" class="game1-2-q"  data-check="0" ondragover="false" ondrop="false"" ><img id="g1-2-nationaldefense-2" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/nationaldefense-down-2.png"></div>');
            $('#box').append('<div id="g1-2-sel-3-5" class="game1-2-q"  data-check="0" ondragover="false" ondrop="false"" ><img id="g1-2-poison-3" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/poison-down-3.png"></div>');
            $('#box').append('<div id="g1-2-sel-3-6" class="game1-2-q"  data-check="0" ondragover="false" ondrop="false"" ><img id="g1-2-nationaldefense-4" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/nationaldefense-down-4.png"></div>');
            $('#box').append('<div id="g1-2-sel-3-7" class="game1-2-q"  data-check="0" ondragover="false" ondrop="false"" ><img id="g1-2-nationaldefense-1" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/nationaldefense-down-1.png"></div>');
            //ans
            $('#box').append('<div id="g1-2-ans-3-001" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-3-004" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-3-006" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-3-007" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-3-002" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-3-003" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-3-005" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            // ans double cover
            $('#box').append('<div id="g1-2-ans-3-016" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-3-023" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            break;
        case '4':
            // bg
            $('#box').append('<img id="g1-2-steel-bg" class="game1-2-q" src="image/game1-2/steel-up-bg.png">');
            $('#box').append('<img id="g1-2-waste-bg" class="game1-2-q" src="image/game1-2/waste-up-bg.png">');
            // sel
            $('#box').append('<div id="g1-2-sel-4-1" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-waste-1" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/waste-down-1.png"></div>');
            $('#box').append('<div id="g1-2-sel-4-2" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-steel-1" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/steel-down-1.png"></div>');
            $('#box').append('<div id="g1-2-sel-4-3" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-waste-2" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/waste-down-2.png"></div>');
            $('#box').append('<div id="g1-2-sel-4-4" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-steel-2" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/steel-down-2.png"></div>');
            $('#box').append('<div id="g1-2-sel-4-5" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-waste-3" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/waste-down-3.png"></div>');
            $('#box').append('<div id="g1-2-sel-4-6" class="game1-2-q" data-check="0" ondragover="false" ondrop="false""><img id="g1-2-steel-3" class="game1-2-q" draggable="true" ondragstart="g1_2_dragstartHandler(event)" src="image/game1-2/steel-down-3.png"></div>');
            // ans
            $('#box').append('<div id="g1-2-ans-4-002" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-4-004" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-4-006" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-4-001" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-4-003" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            $('#box').append('<div id="g1-2-ans-4-005" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            // ans double cover
            $('#box').append('<div id="g1-2-ans-4-013" class="game1-2-q" data-check="0" ondragover="dragoverHandler(event)" ondrop="g1_2_dropHandler(event)"></div>');
            break;
    }
}

//拖曳區
//開始拖曳
function g1_2_dragstartHandler(e) {
    e.dataTransfer.setData('image/jpg', e.currentTarget.id);
    var imgID = e.dataTransfer.getData('image/jpg'); //圖片ID
    //var divSelObj = document.getElementById(imgId); //圖片物件
    var imgObj = e.currentTarget; //***這也是圖片物件***
    var divObj = imgObj.parentElement; //圖片起始DIV
    //divObj.setAttribute("data-check", 0); //有可能上面的DIV也會改??

}
//圖片經過拖曳區
function dragoverHandler(e) {
    e.preventDefault();
}

//圖片放入拖曳區
function g1_2_dropHandler(e) {
    e.preventDefault();
    var imgID = e.dataTransfer.getData('image/jpg'); //圖片ID
    var imgObj = document.getElementById(imgID); //圖片物件
    var startDiv = imgObj.parentElement;
    var ansDiv = e.currentTarget; //目標DIV
    var ImgDivID;
    var newAnsDiv;
    var checkOK = 0;
    //起始DIV ID g1-2-sel-1-8
    //目標DIV ID g1-2-ans-1-08
    //上方圖片 ID chemistry-up-3
    //下方圖片 ID chemistry-down-3
    if (startDiv.id.charAt(startDiv.id.length - 1) == ansDiv.id.charAt(ansDiv.id.length - 1)) {
        checkOK = 1;
        ImgDivID = ansDiv.id.replace("ans", "img").replace(ansDiv.id.charAt(ansDiv.id.length - 3) + ansDiv.id.charAt(ansDiv.id.length - 2) + ansDiv.id.charAt(ansDiv.id.length - 1), "00" + ansDiv.id.charAt(ansDiv.id.length - 1));
        newAnsDiv = ansDiv.id.replace(ansDiv.id.charAt(ansDiv.id.length - 3) + ansDiv.id.charAt(ansDiv.id.length - 2) + ansDiv.id.charAt(ansDiv.id.length - 1), "00" + ansDiv.id.charAt(ansDiv.id.length - 1));

    } else if (startDiv.id.charAt(startDiv.id.length - 1) == ansDiv.id.charAt(ansDiv.id.length - 2)) {
        checkOK = 1;
        ImgDivID = ansDiv.id.replace("ans", "img").replace(ansDiv.id.charAt(ansDiv.id.length - 3) + ansDiv.id.charAt(ansDiv.id.length - 2) + ansDiv.id.charAt(ansDiv.id.length - 1), "00" + ansDiv.id.charAt(ansDiv.id.length - 2));
        newAnsDiv = ansDiv.id.replace(ansDiv.id.charAt(ansDiv.id.length - 3) + ansDiv.id.charAt(ansDiv.id.length - 2) + ansDiv.id.charAt(ansDiv.id.length - 1), "00" + ansDiv.id.charAt(ansDiv.id.length - 2));
    } else if (startDiv.id.charAt(startDiv.id.length - 1) == ansDiv.id.charAt(ansDiv.id.length - 3)) {
        checkOK = 1;
        ImgDivID = ansDiv.id.replace("ans", "img").replace(ansDiv.id.charAt(ansDiv.id.length - 3) + ansDiv.id.charAt(ansDiv.id.length - 2) + ansDiv.id.charAt(ansDiv.id.length - 1), "00" + ansDiv.id.charAt(ansDiv.id.length - 3));
        newAnsDiv = ansDiv.id.replace(ansDiv.id.charAt(ansDiv.id.length - 3) + ansDiv.id.charAt(ansDiv.id.length - 2) + ansDiv.id.charAt(ansDiv.id.length - 1), "00" + ansDiv.id.charAt(ansDiv.id.length - 3));
    }
    if (checkOK == 1) {
        var imgName = GetImgName(imgObj.getAttribute("src")).replace(".png", "").replace('down', 'up');
        $('#' + newAnsDiv).append('<img id="' + ImgDivID + '" class="game1-2-' + g1_2_q_arr[g1_2_level] + '"  src=image/game1-2/' + imgName + '.png>');
        $('#' + imgID).remove(); //remove down-Img
        g1_2_levels_sum(newAnsDiv);
    }
}

//drag結束 可能放入DIV或是回彈
function g1_2_dragendHandler(e) {

}
